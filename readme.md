# Personal i3status config

- [Info](#info)

# Info

- What is i3status ?
    - i3status is a small program for generating a status bar for i3bar, dzen2, xmobar or similar programs.
    - i3status updates every second.
    - [more info](https://i3wm.org/i3status/)

- You can see what is displayed in the status bar and the order in line 14 of [config](/../../blob/main/config).